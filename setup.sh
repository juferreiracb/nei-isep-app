#!/bin/bash
echo "Starting..."

AWS_ACCESS_KEY_ID=$1
AWS_SECRET_ACCESS_KEY=$2
AWS_REGION=$3
DOCKER_IMAGE_AWS_CREDENTIALS_FILE="/aws/credentials"
DOCKER_IMAGE_AWS_CONFIG_FILE="/aws/config"
BUCKET_NAME_SUFFIX=`date +"%F-%H%M%S"`
TFSTATE_TARGET_BUCKET="tf-state-output-$BUCKET_NAME_SUFFIX"
DOCKER_IMAGE_NAME="cloudsanatomy"
CAPABILITIES="CAPABILITY_IAM CAPABILITY_NAMED_IAM CAPABILITY_AUTO_EXPAND"
AWS_ACCOUNT_ID=`aws sts get-caller-identity | jq '.Account' | sed -e 's/^"//' -e 's/"$//'`
DOCKER_REGISTRY="$AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com"

## Writing aws credentials and config file using the docker variables

echo "Writing aws credentials and config file using the docker variables..."

echo "[default]" >> $DOCKER_IMAGE_AWS_CREDENTIALS_FILE
echo "aws_access_key_id = $AWS_ACCESS_KEY_ID" >> $DOCKER_IMAGE_AWS_CREDENTIALS_FILE
echo "aws_secret_access_key = $AWS_SECRET_ACCESS_KEY" >> $DOCKER_IMAGE_AWS_CREDENTIALS_FILE
echo "[default]" >> $DOCKER_IMAGE_AWS_CONFIG_FILE
echo "region = $AWS_REGION" >> $DOCKER_IMAGE_AWS_CONFIG_FILE

### Creating bucket to storage tfstate - we'll use it for terraform destroy
#echo "Creating bucket to storage tfstate (we'll use it for terraform destroy)..."
#aws s3 mb s3://${TFSTATE_TARGET_BUCKET} #TODO: tratativa de erro neste comando
#
#cd ../app/iac
#
##Setting specific targets only for test purposes
##terraform plan -target=aws_ecr_repository.ecr && terraform apply -target=aws_ecr_repository.ecr
#
### Building infrastructure with Terraform
#echo "Building infrastructure with Terraform..."
#terraform init && terraform plan && terraform apply --auto-approve
#
### Moving tfstate for s3 bucket created previously
#echo "Moving tfstate for s3 bucket created previously..."
#aws s3 cp terraform.tfstate s3://${TFSTATE_TARGET_BUCKET}

## ECR Login
echo "ECR Login..."
aws ecr get-login-password | docker login --username AWS --password-stdin $DOCKER_REGISTRY

#Building app docker image and publishing it to AWS ECR
echo "Building app docker image and publishing it to AWS ECR..."
#cd ..
docker build . -t $DOCKER_IMAGE_NAME:latest
docker tag cloudsanatomy:latest $DOCKER_REGISTRY/$DOCKER_IMAGE_NAME:latest
docker push $DOCKER_REGISTRY/$DOCKER_IMAGE_NAME:latest


## Deploying Application inside ECS with Cloudformation
echo "Deploying Application inside ECS with Cloudformation..."
cd cloudformation
## Adjust parameter store entry name
sed -i "s/serviceName/$DOCKER_IMAGE_NAME/g" fargate.yaml
aws cloudformation deploy --stack-name $DOCKER_IMAGE_NAME \
            --template-file fargate.yaml \
            --parameter-overrides file://hiring.json \
            --capabilities $CAPABILITIES

echo "The end."