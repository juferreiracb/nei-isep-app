# GYANT CHALLENGE APP

#
## APP Local Run  
For APP local run, follow nexts steps:  

> #### Run Project's Clone for your workstation: 
> Run Project's Clone over the Link <https://bitbucket.org/juferreiracb/nei-isep-app/src/master/>.  
> ```
> git clone https://juferreiracb@bitbucket.org/juferreiracb/nei-isep-app.git 
> ```
> 
> #
> #### Build image: 
> Run next command to make the build of application. 
> 
> `docker build . -t cloudsanatomy`
> 
> Options used: 
> 
> * `.` - To show that file Dockerfile is on actual path. 
> * `-t` - To show the name and optionally a tag in the 'name:tag' format
> 
> #
> #### List images: 
> Run follow command to list available images. 
> 
> `docker image ls`
> 
> #
> #### Run Container: 
> Run follow command to start container. 
> 
> `docker container run -d -p 80:3000 --name neiisep cloudsanatomy`
> 
> Options used: 
> 
> * `-d` - To run the container in background. 
> * `-p` - To expose port from application to workstation. Format is: external-container-port:internal-container-port 
> * `--name` - To describe a name for your container. 
> * `cloudsanatomy` - In this case, 'cloudsanatomy' is the image that I want to use. 
> 
> #
> #### List Container: 
> Run follow command to list running containers. 
> 
> `docker container ls`
> 
> To list all containers (stoped and running), add '-a': 
> 
> `docker container ls -a`
> 
> #
> #### Access the Application URL: 
> To validade the local application, access the port used to expose the application in your workstation: 
> 
> `http://localhost:80`
> 
> #
> #### Register the new user: 
> To validade the access of the application to the database, run the follow command, and to input a new user in database, over the call of the register's API. 
> 
> `curl -X POST http://localhost:80/users/register --data @payload.json -H 'Content-Type: application/json'`
> 
> Options used: 
> 
> * `-X` - (or `--request`) Specify request command to use
> * `--data` - (or `-d`) HTTP POST data
> * `-H` - (or `--header`) Pass custom header(s) to server
> 
> #
